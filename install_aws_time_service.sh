#!/bin/bash

apt-get -y update

# install chrony and point it at amazon time service
apt-get install -y chrony
# remove default pool server settings to ensure
# only amazon time
sed -i '/^pool/d' /etc/chrony/chrony.conf
cat <<EOT >> /etc/chrony/chrony.conf

# aws time service
server 169.254.169.123 prefer iburst
EOT
systemctl restart chrony


