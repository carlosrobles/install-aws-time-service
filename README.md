# install-aws-time-service

bash script that will install chrony and configure it to sync with the aws time service. this is meant to run on an aws ec2 instance.

###Usage
`./install_aws_time_service.sh`

###Confirm only the AWS time service is listed as a source
`chronyc sources -v`

###Confirm time tracking is working
`chronyc tracking`
